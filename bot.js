const dotenv = require('dotenv');
const { LemmyHttp } = require('lemmy-js-client');
const fs = require('fs');
const path = require('path');
const cron = require('node-cron');

dotenv.config();

const {
  LEMMY_COMMUNITY_INSTANCE,
  LEMMY_COMMUNITY_NAME,
  POST_TITLE,
  POST_CONTENT,
  INITIAL_POST_COUNT,
  LEMMY_LOGIN_INSTANCE,
  LEMMY_LOGIN_USERNAME,
  LEMMY_LOGIN_PASSWORD,
} = process.env;

const DATA_FILE_PATH = path.join('/app', 'data', 'weekly_post_data.json');

let client = new LemmyHttp(LEMMY_LOGIN_INSTANCE);

async function doTask() {
  console.log('Running task');
  
  let loginForm = {
    username_or_email: LEMMY_LOGIN_USERNAME,
    password: LEMMY_LOGIN_PASSWORD,
  };
  let jwt = (await client.login(loginForm)).jwt;

  // Specify the default structure
  const defaultData = { weeklyCount: parseInt(INITIAL_POST_COUNT), postId: null };

  let data;
  try {
    // Try to read the file
    data = JSON.parse(fs.readFileSync(DATA_FILE_PATH, 'utf-8'));
  } catch (error) {
    console.log("File doesn't exist. Set default value");
    data = defaultData;
  }

  let { weeklyCount, postId } = data;

  weeklyCount += 1;
  
  // Fetch community id by name and instance
  const communityId = await getCommunityId(LEMMY_COMMUNITY_NAME, LEMMY_COMMUNITY_INSTANCE, jwt);
  
  if (!communityId) {
    console.error("Community not found");
    return;
  }
  
  // Unpin last weekly thread
  if (postId) {
    const unfeaturePostForm = {
      featured: false,
      feature_type: "Community",
      post_id: postId,
      auth: jwt
    };
    
    const unfeature = await client.featurePost(unfeaturePostForm);
  }

  // Create the new weekly thread
  const createPostForm = {
    name: `${POST_TITLE} #${weeklyCount}`,
    body: POST_CONTENT,
    community_id: parseInt(communityId),
    auth: jwt
  };

  const newPost = await client.createPost(createPostForm);
  
  // Pin new weekly thread
  if (newPost) {
    const newPostId = newPost.post_view.post.id;
    
    const featurePostForm = {
      featured: true,
      feature_type: "Community",
      post_id: newPostId,
      auth: jwt
    };
    
    const feature = await client.featurePost(featurePostForm);
    
    // Save the updated weekly count and post id to file
    data = {
      weeklyCount,
      postId: newPost.post_view.post.id,
    };
    fs.writeFileSync(DATA_FILE_PATH, JSON.stringify(data));
  }
  
  console.log('Task finished');
  
}

async function getCommunityId(communityName, communityInstance, jwt) {
  const searchForm = {
    q: communityName,
    type_: 'Communities',
    sort: 'TopAll',
    page: 1,
    limit: 10,
    auth: jwt
  };

  const searchResult = await client.search(searchForm);

  if (searchResult.communities && searchResult.communities.length > 0) {
    // Filter the search results by the specified instance
    const community = searchResult.communities.find(community => community.community.actor_id.includes(communityInstance));
    
    // If a community on the specified instance is found, return its id
    if (community) {
      return community.community.id;
    }
  }

  return null;
}

// Execute at 20:00 every Sunday
cron.schedule('0 20 * * 7', doTask); 

//  every minute for testing
//cron.schedule('* * * * *', doTask); 




// Execute immediately for debugging
//doTask();
