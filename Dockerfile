# Dockerfile
FROM node:16-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

# Node-Cron runs in the timezone of the host machine
# If you want to override, do the following:
#RUN cp /usr/share/zoneinfo/Europe/London /etc/localtime
#RUN echo "Europe/London" > /etc/timezone

CMD [ "node", "bot.js" ]

